if not modules then
   modules = {}
end

modules ['t-highlight-json'] = {
    version   = 1.1,
    comment   = "Syntax Highlighter for JSON",
    author    = "Christoph Reller",
    copyright = "Christoph Reller",
    license   = "GNU General Public License"
}

local P, R, S, V = lpeg.P, lpeg.R, lpeg.S, lpeg.V

local keywords = [[ true false null ]]

local context = context
local verbatim = context.verbatim
local makepattern = visualizers.makepattern
local high = userdata.high

local handler = visualizers.newhandler {
   startinline  = function() context.JsonSnippet(false,"{") end,
   stopinline   = function() context("}") end,
   startdisplay = function() context.startJsonSnippet() end,
   stopdisplay  = function() context.stopJsonSnippet() end,

   boundary     = function(s) verbatim.JsonSnippetBoundary(s) end,
   keyword      = function(s) verbatim.JsonSnippetKeyword(s) end,
   string       = function(s) verbatim.JsonSnippetString(s) end,
   key          = function(s) verbatim.JsonSnippetKey(s) end,
   number       = function(s) verbatim.JsonSnippetNumber(s) end,
   comment      = function(s) verbatim.JsonSnippetComment(s) end,
}

local spacer = high.spacer
local newline = high.newline

local wordchar = high.lowercase + high.uppercase + high.underscore

local keyword = high.words2patt(keywords, wordchar)

local string =  P('"') * (P('\\\"') + (high.utf8char - high.newline - P('"')))^0 * P('"')

local integer = P('-')^0 * high.digit^1
local fraction = P('.') * high.digit^1
local exponent = S('Ee') * S('-+')^0 * high.digit^1
local number = integer * fraction^0 * exponent^0
local comment = P("//") * (high.utf8char - high.newline)^0

local grammar = visualizers.newgrammar(
   "default",
   {
      "visualizer",

      comment = makepattern(handler, "comment", comment),
      braceOpen = makepattern(handler, "boundary", P('{')),
      braceClose = makepattern(handler, "boundary", P('}')),
      brackOpen = makepattern(handler, "boundary", P('[')),
      brackClose = makepattern(handler, "boundary", P(']')),
      colon = makepattern(handler, "boundary", P(':')),
      comma = makepattern(handler, "boundary", P(',')),
      keyword = makepattern(handler, "keyword", keyword),
      string = makepattern(handler, "string", string),
      key = makepattern(handler, "key", string),
      number = makepattern(handler, "number", number),
      value = V("keyword") + V("number") + V("string") + V("array") + V("object"),
      spacer = makepattern(handler, "space", spacer),
      newline = makepattern(handler, "default", newline),
      whitespace = (V("spacer") + (V("comment")^0 * V("newline")))^0,
      element = V("whitespace") * V("value") * V("whitespace"),
      elements = V("element") * (V("comma") * V("elements"))^0,
      member = V("whitespace") * V("key") * V("whitespace") * V("colon") * V("element"),
      members = V("member") * (V("comma") * V("member"))^0,
      object = V("braceOpen") * (V("members") + V("whitespace")) * V("braceClose"),
      array = V("brackOpen") * (V("elements") + V("whitespace")) * V("brackClose"),


      pattern = V("element") + V("comment") + V("default"),

      visualizer = V("pattern")^1
   }
)

local parser = P(grammar)

visualizers.register("json", { parser = parser, handler = handler, grammar = grammar } )
