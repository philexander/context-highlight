========
 README
========

This is the "Syntax Highlight" module for ConTeXt.

:Author: Christoph Reller
:Email: christoph.reller@gmail.com
:License: GNU General Public License
